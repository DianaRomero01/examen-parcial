from flask import Blueprint, render_template
#from . import db

main = Blueprint('main', __name__)

@main.route('/')
def index():
    return render_template('index.html')

@main.route('/forgot-password')
def forgot_password():
    return render_template('forgot-password.html')

@main.route('/404')
def error():
    return render_template('404.html')

@main.route('/blank')
def blank():
    return render_template('blank.html')

@main.route('/buttons')
def buttons():
    return render_template('buttons.html')

@main.route('/cards')
def cards():
    return render_template('cards.html')

@main.route('/charts')
def charts():
    return render_template('charts.html')

@main.route('/tables')
def tables():
    return render_template('tables.html')

@main.route('/utilities-animation')
def utilities_animation():
    return render_template('utilities-animation.html')

@main.route('/utilities-border')
def utilities_border():
    return render_template('utilities-border.html')

@main.route('/utilities-color')
def utilities_color():
    return render_template('utilities-color.html')

@main.route('/utilities-other')
def utilities_other():
    return render_template('utilities-other.html')