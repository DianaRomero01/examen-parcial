document.getElementById('registrationForm').addEventListener('submit', function(event) {
    var firstName = document.getElementById('exampleFirstName').value;
    var lastName = document.getElementById('exampleLastName').value;
    var email = document.getElementById('exampleInputEmail').value;
    var phoneNumber = document.getElementById('inputPhoneNumber').value;
    var password = document.getElementById('exampleInputPassword').value;
    var repeatPassword = document.getElementById('exampleRepeatPassword').value;

    if (!firstName || !lastName || !email || !phoneNumber || !password || !repeatPassword) {
        alert('Please fill in all required fields.');
        event.preventDefault(); // Prevent the form from being submitted.
    }
});